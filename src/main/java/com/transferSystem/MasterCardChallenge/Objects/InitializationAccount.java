package com.transferSystem.MasterCardChallenge.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object used to initialize a new Account. Only contains the basic initial information.
 * Used for the create Account requests in AccountController class.
 */
public class InitializationAccount {
    @JsonProperty("account-id")
    private int accountid;
    @JsonProperty("balance")
    private int balance;
    @JsonProperty("currency")
    private String currency;

    /**
     * Get AccountId.
     * @return
     */
    public int getAccountid() {
        return accountid;
    }

    /**
     * Set new accountId value.
     * @param accountid
     */
    public void setAccountid(int accountid) {
        this.accountid = accountid;
    }

    /**
     * Gets the current balance value of the account holder.
     * @return
     */
    public int getBalance() {
        return balance;
    }

    /**
     * Updates the balance value.
     * @param balance - the new amount that will be in the account.
     */
    public void setBalance(int balance) {
        this.balance = balance;
    }

    /**
     * Get Currency that is being used by account user.
     * @return
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Set new currency that will be used by Account holder.
     * @param currency - new currency type.
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
