package com.transferSystem.MasterCardChallenge.Objects;

import java.util.Stack;

/**
 * Account object contains all the necessary information a bank account user has, their transaction statements and their balance.
 */
public class Account {
    private int accountid;

    private Balance balance;
    private Stack<Statement> statements;
   public Account(int accountid, int balance, String currency){
        this.accountid = accountid;
       this.balance = new Balance(accountid, balance, currency);
       statements = new Stack<>();

    }

    /**
     * Gets a list of 20 or shorter transaction statements that had occoured for this account.
     * @return - an array of most recent statements.
     */
    public Statement[] getMini(){
        int size;
        if(statements.size() > 20){
            size = 20;
        }
        else size = statements.size();
        Statement[] statementList = new Statement[size];


        for(int i = 0; i < size; i++){
            if(!statements.empty()) {
                Statement tempStatement = statements.pop();
                statementList[i] = tempStatement;
            }
            else break;
        }
        for(int i = 0; i < statementList.length; i++){
            statements.push(statementList[i]);
        }
    return statementList;
    }

    /**
     * Fetches account Id
     * @return - integer value of accountid.
     */
    public int getAccountid() {
        return accountid;
    }


    /**
     * Fetches the Balance object.
     * @return - Balance object (contains balance and currency).
     */
    public Balance getBalance() {
        return balance;
    }

    /**
     * Fetches the stack of statements to perform operations on.
     * @return - Stack of statements.
     */
    public Stack<Statement> getStatements() {
        return statements;
    }

    /**
     * Overwrites the previous stack of statements with new stack of statements.
     * @param statements - new Statement stack.
     */
    public void setStatements(Stack<Statement> statements) {
        this.statements = statements;
    }

    /**
     * adds new statements to stack and performs balance operation depending on if it's DEBIT or CREDIT.
     * @param statement - new statement added to account information.
     */
    public void addNewStatement(Statement statement){
        statements.push(statement);
        if(statement.getType().equals("DEBIT")){
            this.balance.setBalance(this.balance.getBalance() - statement.getAmount());
        }
        else if(statement.getType().equals("CREDIT")) {
            this.balance.setBalance(this.balance.getBalance() + statement.getAmount());
        }

    }
}
