package com.transferSystem.MasterCardChallenge.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;


/**
 * Statement object of a transaction that had occoured between bank accounts.
 */
public class Statement {

    @JsonProperty("account-id")
    private int accountid;
    @JsonProperty("amount")
    private int amount;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("type")
    private String type;

    @JsonProperty("transaction-date")
    @JsonFormat(pattern="yyyy-MM-dd-HH:mm")
    private LocalDateTime transactionDate;


    /**
     * Default constructor for Statement to be used by jackson.
     */
    public Statement(){

    }

    /**
     *
     * @param id - recipients accountid.
     * @param amount - the amount being transfered in transaction.
     * @param currency - the currency that is being used.
     * @param type - the type of transaction, CREDIT or DEBIT.
     * @param transactionDate - The time of the transaction writen in the format yyyy-MM-dd-HH:mm.
     */
    public Statement(int id, int amount, String currency, String type, LocalDateTime transactionDate){
        this.accountid = id;
        this.amount = amount;
        this.currency = currency;
        this.type = type;
        this.transactionDate = transactionDate;
    }


    /**
     * Get function for the recipeints account-id.
     * @return - integer value of the recipients id.
     */
    public int getAccountid() {
        return accountid;
    }

    /**
     * Setting the recipients account-id. Used only if necessary.
     * @param accountid - integer value of recipients id.
     */
    public void setAccountid(int accountid) {
        this.accountid = accountid;
    }

    /**
     *
     * @return - integer value of the amount being transferd in transaction.
     */
    public int getAmount() {
        return amount;
    }

    /**
     *
     * @param amount - Set amount to be the new value of transaction.
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     *
     * @return - Get the type of currency that is being transfered.
     */
    public String getCurrency() {
        return currency;
    }

    /**
     *
     * @param currency - the new type of currency being transfered in transaction.
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     *
     * @return - String of the type of transaction being executed.
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type - the new value of the type of transaction.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return - LocalDateTime formated in yyyy-MM-dd-HH:mm of when the transaction occoured.
     */
    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    /**
     *
     * @param transactionDate - update the time the transaction occoured.
     */
    public void setTransactionDate(LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

}
