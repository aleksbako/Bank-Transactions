package com.transferSystem.MasterCardChallenge.Controllers;

import com.transferSystem.MasterCardChallenge.Errors.InsufficientFunds;
import com.transferSystem.MasterCardChallenge.Errors.InvalidAccountNumber;
import com.transferSystem.MasterCardChallenge.Objects.Account;
import com.transferSystem.MasterCardChallenge.Objects.AccountService;
import com.transferSystem.MasterCardChallenge.Objects.Statement;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/accounts/{id}")
/**
 * Statement Controller handles all requests that are related to transactions.
 */
public class StatementController {

    @PostMapping("/statements/create")
    @ResponseBody
    /**
     * @param id - the provided account id
     * @param statement - the statement of the transaction.
     * @return - String of conformation
     */
    public String createStatement(@PathVariable int id, @RequestBody Statement statement) throws InvalidAccountNumber, InsufficientFunds {

       Account account = AccountService.getAccountById(id);

       if(account != null) { //Error If Account does not exist.
           int recieverid = statement.getAccountid();
           Account reciever = AccountService.getAccountById(recieverid);

           if(reciever == null){ //Error if the reciever doesn't exist.
               throw new InvalidAccountNumber();
           }
           if(account.getBalance().getBalance() == 0){ //Error If there is no money in the account.
              throw new InsufficientFunds();
           }
           else {
               statement.setTransactionDate(LocalDateTime.now());
               Statement recieverStatement = new Statement(id,statement.getAmount(),statement.getCurrency(),"CREDIT", LocalDateTime.now());
               account.addNewStatement(statement);
               reciever.addNewStatement(recieverStatement);
               return "Transaction completed";
           }
       }
        throw new InvalidAccountNumber();
    }
    @GetMapping("statements/mini")
    @ResponseBody
    /**
     * @param id - The provided account-id.
     * @return - an array of 20 transactions for an account.
     * @throw - throws InvalidAccountNumber if user with given account doesn't exist.
     */
    public Statement[] getMiniStatementList(@PathVariable int id) throws InvalidAccountNumber {
        if(AccountService.getAccountById(id) != null) { //Checks that the user exists.
            return AccountService.getAccountById(id).getMini();
        }
        else{
            throw new InvalidAccountNumber();
        }
    }


}
