package com.transferSystem.MasterCardChallenge.Errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * InvalidAccountNumber is an exception which occours when an account number doesn't exist in storage.
 * Provides a httpstatus of bad request when occoured.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidAccountNumber extends Exception{

}
