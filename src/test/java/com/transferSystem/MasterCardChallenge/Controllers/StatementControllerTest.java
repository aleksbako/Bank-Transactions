package com.transferSystem.MasterCardChallenge.Controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.transferSystem.MasterCardChallenge.Errors.InsufficientFunds;
import com.transferSystem.MasterCardChallenge.Errors.InvalidAccountNumber;
import com.transferSystem.MasterCardChallenge.Objects.Account;
import com.transferSystem.MasterCardChallenge.Objects.AccountService;
import com.transferSystem.MasterCardChallenge.Objects.Balance;
import com.transferSystem.MasterCardChallenge.Objects.Statement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class StatementControllerTest {

    private StatementController statementController;

    @BeforeEach
    void SetUp() {
        statementController = new StatementController();
    }

    void AddTemporaryAccount(int id) {
        AccountService.addAccount(new Account(id, 20, "GBP"));
    }

    @Test
    void TestCreateStatementWithInvalidId() {
        assertThrows(InvalidAccountNumber.class, () -> statementController.createStatement(1, null));
    }

    @Test
    void TestCreateStatementWithNoBalance() throws InvalidAccountNumber, InsufficientFunds {

        AddTemporaryAccount(1);
        AccountService.getAccountById(1).getBalance().setBalance(0.0);

        Statement mockStatement = mock(Statement.class);
        when(mockStatement.getAmount()).thenReturn(945);
        when(mockStatement.getAccountid()).thenReturn(1);

        StatementController statementController1 = mock(StatementController.class);


        assertThrows(InsufficientFunds.class, () -> statementController.createStatement(1, mockStatement));
    }

    @Test
    void TestCreateStatementWithInvalidReciever() {
        Statement mockStatement = mock(Statement.class);
        when(mockStatement.getAccountid()).thenReturn(-1);
        assertThrows(InvalidAccountNumber.class, () -> statementController.createStatement(1, mockStatement));
    }

    @Test
    void TestCreateStatementCorrectly() throws InvalidAccountNumber, InsufficientFunds {
        AddTemporaryAccount(1);
        AddTemporaryAccount(2);

        Statement statement = new Statement(2, 10, "GBP", "DEBIT", LocalDateTime.now());
        assertEquals("Transaction completed", statementController.createStatement(1, statement));
    }

    @Test
    void TestgetMiniStatementList() throws InvalidAccountNumber, InsufficientFunds {
        AddTemporaryAccount(1);
        AddTemporaryAccount(2);
        Statement statement = new Statement(2, 10, "GBP", "DEBIT", LocalDateTime.now());
        assertEquals("Transaction completed", statementController.createStatement(1, statement));
        assertNotNull(statementController.getMiniStatementList(1));
    }

    @Test
    void TestgetMiniStatementListInvalidAccount() throws InvalidAccountNumber, InsufficientFunds {
        assertThrows(InvalidAccountNumber.class, () -> statementController.getMiniStatementList(3));
    }
}