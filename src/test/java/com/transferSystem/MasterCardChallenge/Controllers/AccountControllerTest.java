package com.transferSystem.MasterCardChallenge.Controllers;

import com.transferSystem.MasterCardChallenge.Errors.InvalidAccountNumber;
import com.transferSystem.MasterCardChallenge.Objects.AccountService;
import com.transferSystem.MasterCardChallenge.Objects.InitializationAccount;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

class AccountControllerTest {
    @Test
    void TestAccountCreation(){
        InitializationAccount initializationAccount = new InitializationAccount();
        initializationAccount.setAccountid(111);
        initializationAccount.setBalance(3000);
        initializationAccount.setCurrency("GBP");
        AccountController accountController = new AccountController();
        accountController.createBankAccount(initializationAccount);
        assertEquals(initializationAccount.getAccountid(),AccountService.getAccountById(111).getAccountid());
        assertEquals(initializationAccount.getBalance(),AccountService.getAccountById(111).getBalance().getBalance());
        assertEquals(initializationAccount.getCurrency(),AccountService.getAccountById(111).getBalance().getCurrency());

    }

    @Test
    void TestgetAccountBalanceGoodId(){
        AccountController accountController = new AccountController();
        try {
            assertEquals(3000,accountController.getAccountBalance(111).getBalance());
        } catch (InvalidAccountNumber invalidAccountNumber) {
            invalidAccountNumber.printStackTrace();
        }

    }

    @Test
    void TestgetAccountBalanceInvalidId() throws InvalidAccountNumber {

        AccountController accountController = new AccountController();

       assertThrows(InvalidAccountNumber.class, () -> accountController.getAccountBalance(121));
    }
}