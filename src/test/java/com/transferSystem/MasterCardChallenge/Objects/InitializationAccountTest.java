package com.transferSystem.MasterCardChallenge.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InitializationAccountTest {
    private InitializationAccount initializationAccount;
    @BeforeEach
    void SetUp(){
        initializationAccount = new InitializationAccount();
    }
    @Test
    void TestSetAndGetAccountId(){
        initializationAccount.setAccountid(1);
        assertEquals(1,initializationAccount.getAccountid());
    }
    @Test
    void TestSetAndGetBalance(){
        initializationAccount.setBalance(50);
        assertEquals(50,initializationAccount.getBalance());
    }
    @Test
    void TestSetAndGetCurrency(){
        initializationAccount.setCurrency("NOK");
        assertEquals("NOK",initializationAccount.getCurrency());

    }
}