package com.transferSystem.MasterCardChallenge.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BalanceTest {
    private Balance balance;

    @BeforeEach
    void SetUp(){
        balance = new Balance(1,23.0,"EURO");
    }
    @Test
    void TestSetandGetBalance() {
        balance.setBalance(102.5);
        assertNotEquals(23.0,balance.getBalance());
    }

    @Test
    void TestSetandGetCurrency() {
        balance.setCurrency("USD");
        assertNotEquals("EURO",balance.getCurrency());
    }


}